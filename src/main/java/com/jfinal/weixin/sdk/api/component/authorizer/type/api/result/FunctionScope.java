package com.jfinal.weixin.sdk.api.component.authorizer.type.api.result;

public class FunctionScope {
    private Category funcscope_category;

    public Category getFuncscope_category() {
        return funcscope_category;
    }

    public void setFuncscope_category(Category funcscope_category) {
        this.funcscope_category = funcscope_category;
    }
}
