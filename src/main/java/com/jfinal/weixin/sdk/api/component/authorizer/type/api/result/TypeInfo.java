package com.jfinal.weixin.sdk.api.component.authorizer.type.api.result;

public class TypeInfo {
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}