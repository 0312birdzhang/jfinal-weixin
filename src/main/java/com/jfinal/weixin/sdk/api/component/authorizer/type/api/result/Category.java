package com.jfinal.weixin.sdk.api.component.authorizer.type.api.result;

public class Category {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
